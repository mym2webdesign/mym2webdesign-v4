<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="full_width container_shadow">
			<header><h2><?php //get page section title
				if (get_post_meta($post->ID, $shortname.'_title_bar',true)) {
						echo get_post_meta($post->ID, $shortname.'_title_bar',true);
				} else { 
					echo $post->post_title; 
				} 
			?></h2></header>	
		</section>

		<section id="content" class="full_width tour-row clearfix">
			<article class="comments">
        <header>
          <h3>All about Wordpress.</h3>
          <p>
            You can certainly say that we are in love with Wordpress. 
          </p>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
          <a class="btn btn-large btn-fresh" href="#"><i class="icon-chevron-right icon-white"></i> Our Work</a> <a class="btn btn-large btn-fresh" href="#"><i class="icon-chevron-right icon-white"></i> Our Themes</a>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

		<section id="content" class="full_width tour-row-alt clearfix">
			<article class="comments">
        <header>
          <h3>Mobile apps.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

		<section id="content" class="full_width tour-row clearfix">
			<article class="comments">
        <header>
          <h3>Social media.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

		<section id="content" class="full_width tour-row-alt clearfix">
			<article class="comments">
        <header>
          <h3>Media design.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

		<section id="content" class="full_width tour-row clearfix">
			<article class="comments">
        <header>
          <h3>Print design.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

		<section id="content" class=" full_width tour-row-alt clearfix">
			<article class="comments">
        <header>
          <h3>Wordpress training.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
			</article>
		</section>

    <section id="content" class="full_width tour-row clearfix">
      <article class="comments">
        <header>
          <h3>Support on demand.</h3>
          <p>
            Real-time posting and threading makes comment activity easy to engage with. Our comment moderating and spam filtering tools have never been more powerful. Your readers can login using Twitter and Facebook, and you can integrate Disqus. 
          </p>
        </header>
        <img class="main loaded" data-src="http://www.squarespace.com/assets/tour-comments.png" style="" data-image-dimensions="468x560" src="http://www.squarespace.com/assets/tour-comments.png?format=500w" data-image="/assets/tour-comments.png" alt="Squarespace Comments">
      </article>
    </section>
		
  </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
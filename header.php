<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>">
<title><?php global $shortname, $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' ); // Add the blog name.
	$site_description = get_bloginfo( 'description', 'display' ); // Add the blog description for the home/front page.
	if ( $site_description && ( is_home() || is_front_page() ) ) { echo " | $site_description"; }

	if ( $paged >= 2 || $page >= 2 ) { echo ' | ' . sprintf( __( 'Page %s', 'goodminimal' ), max( $paged, $page ) ); } // Add a page number if necessary:
	?></title>

<?php if (get_option($shortname.'_favicon_url')) { ?>
<link rel="shortcut icon" href="<?php echo get_option($shortname."_favicon_url"); ?>">
<?php } else { ?>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<?php } ?>

<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

<?php
	if (get_option($shortname.'_theme_style')) {
		$theme_style = get_option($shortname.'_theme_style');
	} else {
		$theme_style = 'light-style.css';
	}

	$theme_color = str_replace('-style.css','',$theme_style);
?>
<link href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $theme_style; ?>" type="text/css" rel="stylesheet" media="screen">

<link href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" rel="stylesheet" media="screen">
<link href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $theme_color; ?>-tipsy.css" type="text/css" rel="stylesheet" media="screen">
<link href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $theme_color; ?>-nivo-slider.css" type="text/css" rel="stylesheet" media="screen">

<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>

<script src="<?php echo get_template_directory_uri(); ?>/js/libs/modernizr-1.7.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery.tweet.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<script type="text/javascript">
/*NIVO SLIDER*/
$(window).load(function() {
	$('#slider').nivoSlider({
			effect:'<?php echo get_option($shortname.'_nivo_effect'); ?>', //Specify sets like: 'fold,fade,sliceDown, random'
			animSpeed:<?php echo get_option($shortname.'_animSpeed'); ?>,
			pauseTime:<?php echo get_option($shortname.'_pauseTime'); ?>,
			directionNav:<?php if (get_option($shortname.'_directionNav')) { echo get_option($shortname.'_directionNav'); } else { echo 'false'; } ?>,
			controlNav:<?php if (get_option($shortname.'_controlNav')) { echo get_option($shortname.'_controlNav'); } else { echo 'false'; } ?>,
			keyboardNav:<?php if (get_option($shortname.'_keyboardNav')) { echo get_option($shortname.'_keyboardNav'); } else { echo 'false'; } ?>,
			pauseOnHover:<?php if (get_option($shortname.'_pauseOnHover')) { echo get_option($shortname.'_pauseOnHover'); } else { echo 'false'; } ?>
	});
});
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery.tipsy.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs/css3-mediaqueries.js" type="text/javascript"></script>

<!-- PrettyPhoto-->
<link  rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $theme_color; ?>-prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" >
<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery.prettyPhoto.js" type="text/javascript"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js" type="text/javascript"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/libs/hoverIntent.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs/superfish.js" type="text/javascript"></script>

</head>

<?php $body_id = ( is_home() || is_front_page() ) ? 'id="outer"' : 'id="inner"'; ?>
<body <?php echo $body_id; ?>>
<div id="page">

<header id="header">
	<div id="topbar" class="clearfix">
		<div class="inner clearfix">
			<p id="tagline"><?php bloginfo('description'); ?></p>
			<div class="pull-right">
				<div id="member-info" style="display: block;">
					<?php if (is_user_logged_in()) : ?>
					Welcome, <?php global $current_user; get_currentuserinfo(); echo '' . $current_user->display_name . "\n";?> | <a href="http://www.mym2webdesign.com/profile/">Profile</a> | <a href="<?php echo wp_logout_url( home_url() ); ?>" title="Logout">Logout</a>
					<?php else : ?>
					<a href="http://www.mym2webdesign.com/login/">Sign-in</a> or <a class="create-account" href="http://www.mym2webdesign.com/login/">Create an Account</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
  <div id="header_inner" class="clearfix">
		<?php 
			if (get_option($shortname.'_logo')) { 
			$logo_url = get_option($shortname.'_logo');
			$logo_width = get_option($shortname.'_logo_width');
			$logo_height = get_option($shortname.'_logo_height');
  		  ?>
			<h1 id="logo"><a style="background:url(<?php echo $logo_url; ?>) no-repeat;	width:<?php echo $logo_width; ?>px; height:<?php echo $logo_height; ?>px;" href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
			<?php } else { ?>
			<h1 id="logo"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
        <?php } ?>
    	<nav>

		    <?php
				//begin to display WP 3 Menus or default menu
				$menu_manager = get_option($shortname.'_menu_manager');
				if (!$menu_manager) {
					wp_nav_menu( array(
					'menu'              => '',
					'container'         => '',
					'container_class'   => '',
					'container_id'      => '',
					'menu_class'        => '',
					'menu_id'           => '',
					'echo'              => true,
					'fallback_cb'       => 'fallback_no_menu',
					'before'            => '',
					'after'             => '',
					'link_before'       => '',
					'link_after'        => '',
					'depth'             => 0,
					'walker'            => '',
					'theme_location'    => ''
					)
				);	
			} else {
		    ?>	
				  <ul class="sf-menu">	
						<li><a href="<?php echo home_url(); ?>">Home</a></li>
					  <?php	
						$page_exclusions = get_option($shortname.'_exclude_header_pages');
						$nav_menu_pages = wp_list_pages('show_home=1&sort_column=menu_order&sort_order=asc&exclude=&title_li=&echo=0');
						$nav_menu_pages = str_replace('current_page_item','current_page_item active',$nav_menu_pages);
						echo $nav_menu_pages;
						} //end to check if default menu is enabled 
						?>
					</ul>
        </nav>
    </div><!-- END: #header_inner-->
    
    <!-- Then somewhere in the <body> section -->
</header><!-- END: #header-->

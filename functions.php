<?php
/* goodminimal functions and definitions */
/* Set the content width based on the theme's design and stylesheet. Used to set the width of images and content. */
if ( ! isset( $content_width ) )
	$content_width = 640;

/* Run goodminimal_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'goodminimal_setup' );

if ( ! function_exists( 'goodminimal_setup' ) ):

	function goodminimal_setup() {

		// This theme uses post thumbnails
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 250, 200, true ); 
		add_image_size( 'showcase-thumb', 270, 200, true );
		add_image_size( 'homepageslider', 960, 447, true );

		if (!is_admin()) {
			wp_deregister_script('jquery');
			wp_register_script('jquery',  get_template_directory_uri().'/js/libs/jquery-1.5.1.min.js', false, '1.5.1');
			wp_enqueue_script('jquery');
		}
		
		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Make theme available for translation. Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'goodminimal', TEMPLATEPATH . '/languages' );
		$locale = get_locale();
		$locale_file = TEMPLATEPATH . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );		
		
		// This theme uses wp_nav_menu() in one location.
		add_theme_support('nav-menus');
		if ( function_exists( 'register_nav_menus' ) ) {
			register_nav_menus(
				array(
				  'header_menu' => 'Header Navigation'
				)
			);
		}

		
		// Style the visual editor.
		add_editor_style();
		
		// This theme allows users to set a custom background
		add_custom_background();
	}
endif;

function custom_bbp_show_lead_topic( $show_lead ) {
  $show_lead[] = 'true';
  return $show_lead;
}
 
add_filter('bbp_show_lead_topic', 'custom_bbp_show_lead_topic' );

add_filter( 'bbp_get_single_forum_description', 'ja_return_blank' );

add_filter( 'bbp_get_single_topic_description', 'ja_return_blank' );

function ja_return_blank() {

    return '';

}

function goodminimal_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'goodminimal_page_menu_args' );

function goodminimal_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'goodminimal_excerpt_length' );


function goodminimal_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'goodminimal_remove_gallery_css' );

if ( ! function_exists( 'goodminimal_comment' ) ) :

function goodminimal_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
	<div class="comment comment" id="comment-<?php comment_ID(); ?>">
		
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'goodminimal' ); ?></em>
			<br />
		<?php endif; ?>

		
		<h5><?php comment_author_link(); ?></h5>
        <span class="date"><?php
			/* translators: 1: date, 2: time */
			printf( __( '%1$s', 'goodminimal' ), get_comment_date() ); ?></a><?php edit_comment_link( __( '(Edit)', 'goodminimal' ), ' ' );
		?></span>
        <p><?php comment_text(); ?></p>
		<?php comment_reply_link( array_merge( $args, array( 'class' => 'comment-reply-link', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		  
	</div>
	</li>

	<?php
	endswitch;
}
endif;


function goodminimal_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'goodminimal_remove_recent_comments_style' );

if ( ! function_exists( 'goodminimal_posted_on' ) ) :

function goodminimal_posted_on() {	
	?>
	<span class="meta"><?php _e( 'Written by', 'goodminimal' ); ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author(); ?></a> <?php _e( 'on', 'goodminimal' ); ?> <a href="<?php echo get_month_link(get_the_time('Y'),get_the_time('m')).get_the_time('d'); ?>"><?php echo get_the_date('M jS, Y'); ?></a> <?php _e( 'in', 'goodminimal' ); ?> <?php the_category(', '); ?> | <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span> 	
	<?php
}
endif;

if ( ! function_exists( 'goodminimal_posted_in' ) ) :

function goodminimal_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goodminimal' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goodminimal' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'goodminimal' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

function graphene_author_nolink(){
   return get_the_author();
}
add_filter( 'the_author_posts_link', 'graphene_author_nolink' );

/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/
/******************************* BEGIN ADMIN PANEL *********************************************/
/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/

define('ADMIN_JS',  get_template_directory_uri() . '/functions/options/js' );
define('ADMIN_CSS',  get_template_directory_uri() . '/functions/options/css' );
define('ADMIN_OPTIONS', TEMPLATEPATH . '/functions/options');
define('GENERAL_FUNCTIONS', TEMPLATEPATH . '/functions');

function admin_scripts() {
	//wp_enqueue_script('color-picker', ADMIN_JS.'/jscolor/jscolor.js', array('jquery'));
	wp_enqueue_script('slider-table', ADMIN_JS .'/slider_table.js', false, '1.0.0');
	wp_enqueue_script('slide-toggle-menu', ADMIN_JS .'/slide_toggle_menu.js', false, "1.0.0");
	wp_enqueue_script('slide-add-remove', ADMIN_JS .'/slider_add_remove.js', false, "1.0.0");
	wp_enqueue_style( 'slider-css', ADMIN_CSS .'/slider_table_style.css', false, '1.0.0', 'screen' );
}
add_action('admin_enqueue_scripts', 'admin_scripts');


// Load Posts Custom Fields in Posts
require_once(ADMIN_OPTIONS . '/post-options.php');

// Load Posts Custom Fields in Pages
require_once(ADMIN_OPTIONS . '/page-options.php');

// Load Products
//require_once(ADMIN_OPTIONS . '/products-options.php');

// Load WP Options
require_once(GENERAL_FUNCTIONS . '/custom.php');

// Load WP-Pagenavi
require_once(GENERAL_FUNCTIONS . '/wp-pagenavi.php');

// Load Shortcodes
require_once( GENERAL_FUNCTIONS . '/shortcodes.php');

// Load Widgets
require_once( GENERAL_FUNCTIONS . '/widgets.php');
require_once( GENERAL_FUNCTIONS . '/widgets/recent-posts-widget.php');
require_once( GENERAL_FUNCTIONS . '/widgets/popular-posts-widget.php');
require_once( GENERAL_FUNCTIONS . '/widgets/twitter.php');
require_once( GENERAL_FUNCTIONS . '/widgets/flickr.php');
require_once( GENERAL_FUNCTIONS . '/widgets/social-links.php');
require_once( GENERAL_FUNCTIONS . '/widgets/search.php');
require_once( GENERAL_FUNCTIONS . '/widgets/categories.php');


$themename = "MYM2";
$shortname = "gm";

/* Get CSS files into a array list */
$css_styles = array();
if(is_dir(TEMPLATEPATH . "/css/")) 
{
	if($open_dirs = opendir(TEMPLATEPATH . "/css/")) 
	{
		while(($style = readdir($open_dirs)) !== false) 
		{
			if(stristr($style, "superfish") == false) 
			//if(stristr($style, "reset") == false) 
			{
				if(stristr($style, "-style.css") !== false) 
				{
					$css_styles[] = $style;
				}
			}
		}
	}
}
$css_styles_list = $css_styles;

/* Get Pages into a drop-down list */
/*$pages = get_pages();
$pagetomenu = array();
foreach($pages as $apag) 
{
	$pagetomenu[$page->ID] = $page->post_title;
}*/


$css_styles = array("light-style.css","dark-style.css");

$NivoEffects = array("random","fold","fade","sliceDown");

/* Control Panel options */
$options = array (
	
	array( "type" => "options_begin"),
	
	/* General Theme Settings */
	array( "name" => "General Theme Settings",
	"type" => "toggle"),
	
	array( "type" => "open"),
		
	array(	"name" => "Theme Stylesheet",
			"desc" => "Please choose one of the Theme skins here.",
			"id" => $shortname."_theme_style",
			"type" => "select",
			"options" => $css_styles),
	
	array(	"name" => "Logo URL",
			"desc" => "Logo width:170px; height:40px. If the input field is left blank then the themes default logo gets applied.<br>
			Paste the full URL path to your logo e.g. 'http://www.yourdomain.com/images/logo.jpg'.",
            "id" => $shortname."_logo",
            "type" => "text_image_url"),

	array(	"name" => "Logo Image Width",
			"desc" => "Enter logo image width.",
            "id" => $shortname."_logo_width",
            "type" => "text_image_url",
			"std" => ""),

	array(	"name" => "Logo Image Height",
			"desc" => "Enter logo image height, maximum logo height 64px.",
            "id" => $shortname."_logo_height",
            "type" => "text_image_url",
			"std" => ""),
	
	array(	"name" => "Favicon URL",
			"desc" => "Enter your site favicon url.",
            "id" => $shortname."_favicon_url",
            "type" => "text_image_url",
			"std" => ""),		
			
	array(	"name" => "Activate default WP Menu",
			"desc" => "Check to deactivate <strong>Menu Manager</strong> - Powered by WordPress 3+ system and activate classic menu with pages.",
            "id" => $shortname."_menu_manager",
            "type" => "checkbox"),

	array(	"name" => "Your email",
			"desc" => "Enter your email for contact form.",
			"id" => $shortname."_contact_admin_email",
			"type" => "text"),
			
	array(    "type" => "close"),	
	
	/* Nivo Slider Settings */
	array( "name" => "Nivo Slider Settings",
			"type" => "toggle"),
	array(	"type" => "open"),
	array(	"name" => "effect",
			"id" => $shortname."_nivo_effect",
			"desc" => "Select Nivo effect.",
			"type" => "select_tweenType",
			"options" => $NivoEffects),
	array(  "name" => "animSpeed",
			"desc" => "Set animation speed",
			"id" => $shortname."_animSpeed",
			"type" => "text",
			"std" => "1500"),	
	array(  "name" => "pauseTime",
			"desc" => "Set pause time",
			"id" => $shortname."_pauseTime",
			"type" => "text",
			"std" => "5000"),	
	array(	"name" => "directionNav",
			"desc" => "Activate directionNav",
            "id" => $shortname."_directionNav",
            "type" => "checkbox"),					
	array(	"name" => "controlNav",
			"desc" => "Activate controlNav",
            "id" => $shortname."_controlNav",
            "type" => "checkbox",
			"std" => "true"),	
	array(	"name" => "keyboardNav",
			"desc" => "Activate keyboardNav",
            "id" => $shortname."_keyboardNav",
            "type" => "checkbox",
			"std" => "true"),	
	array(	"name" => "pauseOnHover",
			"desc" => "Activate pauseOnHover",
            "id" => $shortname."_pauseOnHover",
            "type" => "checkbox",
			"std" => "true"),	
	array(  "type" => "close"),

	
	/* Sidebars */
	array( "name" => "Sidebars",
			"type" => "toggle"),	
			
	array( "type" => "open"),
	
	array(	"name" => "Add, Edit, Remove sidebars",
			"desc" => "Add new sidebars for posts, portfolio items, pages anh when edit pages, posts thene select the sidebar.<br/>In widgets you can add info for this sidebar.<br/><br/>
						<strong>Note:</strong> To delete one sidebar, just leave it empty and click button <strong>Save changes</strong>.",
			"id" => $shortname."_sidebar_list",
			"type" => "sidebar_list"),	
			
	array(  "type" => "close"),	
	
	
	/* Footer Theme Settings */
	array( "name" => "Footer Settings",
	"type" => "toggle"),
	
	array( "type" => "open"),
	
	array(	"name" => "Tracking Code",
			"desc" => "Enter the tracking code. Where can I find my tracking code from within my Google Analytics account? Click <a href='http://www.google.com/support/analytics/bin/answer.py?hl=en&answer=55603' target='_blank'>here</a>.",
			"id" => $shortname."_tracking_code",
			"type" => "textarea"),

	array(	"name" => "Footer (Copyright)",
			"desc" => "Enter in the company that is copyrighting site content. This will show up in the footer.",
			"id" => $shortname."_footer_copyright",
			"type" => "textarea",
			"std" => "Copyright 2011. All Rights Reserved."),

	array(    "type" => "close"),
	
	array( "type" => "options_end"),
	
);	
	
function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {
				
                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) 
				{
                    if( isset( $_REQUEST[ $value['id'] ] ) ) 
					{ 
						update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); 
					} else { 
						delete_option( $value['id'] ); 
					} 
				}

				/* Updates unlimited sidebars settings */
				$sidebarsCount = 0;
				foreach ($_POST as $key => $value) 
				{
					if ( preg_match("/sidebar_list_url_/", $key) ) 
					{
						if ($value != '') $sidebarsCount = $sidebarsCount +1;
					}
				}		
				foreach ($_POST as $key => $value) 
				{
					if ( preg_match("/sidebar_list_url_/", $key) ) 
					{	
						if ($value != '') $options_sidebars_custom[$key] = $value;
					}	
					
					$options_sidebars_custom['sidebarsCount'] = $sidebarsCount;
					
					delete_option( 'gm_sidebar_list');
					update_option( 'gm_sidebar_list', $options_sidebars_custom);					
				}

				// Update option 'portfolio_page_id' Portfolio ID to Category for unlimited portfolio subcategories
				foreach ($_POST as $key => $value) 
				{
					if ( preg_match("/portfolio_to_cat_/", $key) ) 
					{	
						if ($value != '') $portfolio_items[$key] = $value;
					}	
									
					delete_option( $shortname.'_portfolio_page_id');
					update_option( $shortname.'_portfolio_page_id', $portfolio_items);					
				}
								
				
                header("Location: themes.php?page=functions.php&saved=true");
                die;

        } else if ( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=functions.php&reset=true");
            die;
        }
    }
    add_menu_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');
}

function categories_for_gallery($shortname,$page_id) {

	//get selected page_id for portfolio
	$get_custom_options = get_option($shortname.'_gallery_page_id'); 
	$get_page_id  = $get_custom_options['gallery_id_'.$page_id];
	
	//get category name
	$get_category_name = get_option($shortname.'_gallery_id_'.$page_id);
	
	$output = '';
	$output .= '<select name="gallery_id_'.$page_id.'" class="postform selector">';	
	$output .= '<option value="0">&nbsp;&nbsp;&nbsp;Select category</option>';	
	$categories = get_categories();			
	foreach ($categories as $cat) 
	{
		$selected_option = $cat->cat_ID;
		if ($get_page_id == $selected_option) { 
			$output .= '<option selected value="'.$cat->cat_ID.'">&nbsp;&nbsp;&nbsp;'.$cat->cat_name.'</option>';
		} else {
			$output .= '<option value="'.$cat->cat_ID.'">&nbsp;&nbsp;&nbsp;'.$cat->cat_name.'</option>';
		}	
	}	
	$output .= '</select>';	
	
	return $output;
}



function check_gallery_template($pagetemplate = '') {
	global $wpdb;
	global $shortname;
	$output = '';
	$output .= '<table>';
	$sql = "select post_id from $wpdb->postmeta where meta_key like '_wp_page_template' and meta_value like '" . $pagetemplate . "'";
	$result = $wpdb->get_results($sql);
	foreach ($result as $value){
		$page_id = $value->post_id;
		$page_data = get_page( $page_id );
		$title = $page_data->post_title; 
		$output .=  '<tr><td>Select Category to display in  Page <strong>'.$title.'</strong></td><td>&nbsp;:&nbsp;</td><td>'.categories_for_gallery($shortname,$page_id).'</td></tr>';
	}
	$output .= '</table>';
	
	return $output;
}


function display_contact_content($values_id,$shortname,$blog_or_news)
{
	$get_blog_name = get_option($shortname.'_display_'.$blog_or_news.'_content');

	echo '<select name="'.$values_id.'" class="postform selector">';	
	echo '<OPTGROUP LABEL="Pages">';
	echo '<option value="0">Select Page</option>';		
	global $post;
	$myposts = get_pages();
	foreach($myposts as $post) : setup_postdata($post);
		$selected_option = $post->ID;		
		if ( $get_blog_name == $selected_option ) { 
		?>
			<option selected value='<?php echo $post->ID; ?>'>&nbsp;&nbsp;&nbsp;<?php the_title(); ?></option>";
		<?php	
		}
		if ( $get_blog_name != $selected_option ) { 
		?>
			<option value='<?php  echo $post->ID; ?>'>&nbsp;&nbsp;&nbsp;<?php the_title(); ?></option>";
		<?php 
		}
	endforeach;
	echo '</OPTGROUP>';	
	echo '</select>';
}

function display_news_content($values_id,$shortname,$blog_or_news)
{
	$get_blog_name = get_option($shortname.'_display_'.$blog_or_news.'_content');

	
	if ($blog_or_news == 'news'){
		$get_news_name = get_option($shortname.'_display_'.$blog_or_news.'_content_to_cat');
		echo '<select name="'.$values_id.'_to_cat" class="postform selector">';	
		echo '<OPTGROUP LABEL="Categories">';
		echo '<option value="0">Select category</option>';			
		$categories = get_categories();			
		foreach ($categories as $cat) 
		{
			$selected_option = $cat->cat_ID;
			if ($get_news_name == $selected_option) { 
			?>
			
				<option selected value='<?php echo $cat->cat_ID; ?>'>&nbsp;&nbsp;&nbsp;<?php echo $cat->cat_name; ?></option>			
			<?php
			} else {
			?>
			
				<option value='<?php echo $cat->cat_ID; ?>'>&nbsp;&nbsp;&nbsp;<?php echo $cat->cat_name; ?></option>
			<?php 
			}	
		};			
		echo '</OPTGROUP>';	
		echo '</select>';	
	}			
}

/* Exclude/Include pages from/in header menu */
function exclude_header_pages($values_id,$shortname)
{
	$htmlselected = '';
	$get_custom_options = get_option($shortname.'_exclude_header_pages');
	
	$page_items = explode(',',$get_custom_options);
	$count_pages = count($page_items);
	foreach($page_items as $page_item){
		$page_item_list[] = $page_item;
	}

	$n = 0;
	$n2 = 777;
	global $post;
	$arguments = array(
		'child_of' =>  $n,
		'parent' => $n
	);

	//$pages = get_pages();
	$myposts = get_pages($arguments);
	foreach($myposts as $post) : setup_postdata($post);
		$selected_option = get_permalink($post->ID);

		$checked_page = '';
		for($i=0;$i<$count_pages;$i++)
		{
			if ($page_item_list[$i] == $post->ID) { $checked_page = 'checked="yes"'; }
		}
		$n = $n + 1;
		if ($n == 10) { echo '<br>'; $n = 1;}
		echo '<p style="display:inline; padding: 0 10px 0 3px;">&raquo;<input '.$checked_page.' onClick="getSelectValue_pages('.$post->ID.');" name="'.$post->ID.'" type="checkbox" value="'.the_title().'"/></p>';
	endforeach;	
	
	echo '<p><input type="hidden" readonly="readonly" style="padding-top: 4px; width: 400px;"  name="'.$values_id.'" id="'.$values_id.'" value="'.$get_custom_options.'" type="text"></p>';
}

function display_gallery_portfolio_services($values_id,$shortname,$gallery_or_portfolio)
{
	$get_blog_name = get_option($shortname.'_display_'.$gallery_or_portfolio.'_content');

	$get_gallery_name = get_option($shortname.'_display_'.$gallery_or_portfolio.'_content_to_cat');
	echo '<select name="'.$values_id.'_to_cat" class="postform selector">';	
	echo '<OPTGROUP LABEL="Categories">';
	echo '<option value="0">&nbsp;&nbsp;&nbsp;Select category</option>';	
	$categories = get_categories();			
	foreach ($categories as $cat) 
	{
		$selected_option = $cat->cat_ID;
		if ($get_gallery_name == $selected_option) { 
		?>
		
			<option selected value='<?php echo $cat->cat_ID; ?>'>&nbsp;&nbsp;&nbsp;<?php echo $cat->cat_name; ?></option>			
		<?php
		} else {
		?>
		
			<option value='<?php echo $cat->cat_ID; ?>'>&nbsp;&nbsp;&nbsp;<?php echo $cat->cat_name; ?></option>
		<?php 
		}	
	};			
	echo '</OPTGROUP>';	
	echo '</select>';		
}

/* Exclude categories from Blog */
function exclude_categories($values_id,$shortname)
{
	$get_custom_options = get_option($shortname.'_exclude_categories');
	
	$cat_items = explode(',',$get_custom_options);
	$count_cat = count($cat_items);
	foreach($cat_items as $cat_item){
		$cat_item_list[] = $cat_item;
	}

	$categories = get_categories('hide_empty=0');
	foreach($categories as $cat)
	{
		$checked_cat = '';
		for($i=0;$i<$count_cat;$i++)
		{
			if ($cat_item_list[$i] == $cat->cat_ID) { $checked_cat = 'checked="yes"'; }
		}
		$n = $n + 1;
		if ($n == 10) { echo '<br>'; $n = 1;}
		echo $cat->cat_name ; 
		echo '<p style="display:inline; padding: 0 10px 0 3px;">&raquo;<input '.$checked_cat.' onClick="getSelectValue_cats('.$cat->cat_ID.');" name="'.$cat->cat_ID.'" type="checkbox" value="'.$cat->cat_name.'"/></p>';
	};
	
	echo '<p><input type="hidden" readonly="readonly" style="padding-top: 4px; width: 400px;"  name="'.$values_id.'" id="'.$values_id.'" value="'.$get_custom_options.'" type="text"></p>';
}


/* mytheme_admin function */
function mytheme_admin() {
    global $themename, $shortname, $options;
    if ( $_REQUEST['saved'] ) {	echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>'; }
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';   
?>
<div class="wrap">

<?php screen_icon('options-general'); ?>
<h2><?php echo $themename; ?> settings</h2>

<form method="post" action="">
<?php foreach ($options as $value) { 
    
	switch ( $value['type'] ) {
	
	
		case "options_begin":
		?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/functions/options/js/jquery.tzCheckbox/jquery.tzCheckbox.css" />
		<?php break;
		
		case "options_end":
		?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/functions/options/js/jquery.tzCheckbox/jquery.tzCheckbox.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/functions/options/js/jquery.tzCheckbox/script.js"></script>
		<?php break;
		
		case "open":
		?>
        <table width="100%" border="0" style="background-color:#ffffff; padding:10px;border:1px double #f1f1f1;">  
		<?php break;
		
		case "close":
		?>	
		<tr>
			<td coslapan="2">
			<p class="submit">
				<input name="save" type="submit" value="Save changes" />    
				<input type="hidden" name="action" value="save" />
			</p>
			</td>
		</tr>
        </table><br />     		
		</div>
		</div>
		<?php break;
		
		case "title":
		?>
		<table width="100%" border="0" style="background-color:#f1f1f1; padding:5px 10px;"><tr>
        	<td colspan="2"><h3 style="color:#414141"><?php echo $value['name']; ?></h3></td>
        </tr>       
		<?php break;
		
		case 'text_image_url':
		?>  
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo trim(str_replace('\t','',str_replace('\\', '', get_option( $value['id'] )))); } else { echo $value['std']; } ?>" /></td>
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;

		case 'text':
		?>      
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo trim(str_replace('\t','',str_replace('\\', '', get_option( $value['id'] )))); } else { echo $value['std']; } ?>" /></td>
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;
		
		case "text_translate":
		?>      
        <tr>
			<td colspan="2" width="100%"><h1><?php echo $value['name']; ?></h1><?php echo $value['desc']; ?><hr></td>
        </tr>
		
        <tr>
            <td></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>      
		<?php break;
	
		case 'textarea':
		?>      
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><textarea name="<?php echo $value['id']; ?>" style="width:400px; height:200px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php 
				if ( get_option( $value['id'] ) != "") { 
						echo trim(str_replace('\t','',str_replace('\\', '', get_option( $value['id'] ))));
				} else { 
					echo $value['std']; 
				} 
			?></textarea></td>
         </tr>
        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;
				
		case 'select':
		?>
		<tr>
			<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
			<td width="80%"><select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php 
			echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php 
			if ( get_option( $value['id'] ) == $option) { echo ' selected="selected"'; } 
			else if (get_option( $value['id'] ) == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select>
			</td>
		</tr>
		<tr>
			<td><small><?php echo $value['desc']; ?></small></td>
		</tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php
		break;
		
		case 'select_tweenType':
		?>
		<tr>
			<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
			<td width="80%">
				<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
				<?php foreach ($value['options'] as $option) { ?>
				<option <?php if (get_option( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td><small><?php echo $value['desc']; ?></small></td>
		</tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php
		break;
      
		case "checkbox":
		?>
		<tr>
			<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; } else { if ( $value['std'] === "true" ){ $checked = "checked=\"checked\""; } else { $checked = ""; } } ?>
                        <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
			</td>
        </tr>          
        <tr>
			<td><small><?php echo $value['desc']; ?></small></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
        <?php 		
		break;
		
				
		
		case "radio":
		?>
		<tr>
			<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
			<td width="80%">
				<label><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="radio" value="<?php echo $value['value']; ?>" <?php echo $selector; ?> <?php if (get_option( $value['id']) == $value['value'] || get_option( $value['id']) == ""){echo 'checked="checked"';}?> /> <?php echo $value['desc']; ?></label><br />
				<label><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>_2" type="radio" value="<?php echo $value['value2']; ?>" <?php echo $selector; ?> <?php if (get_option( $value['id']) == $value['value2']){echo 'checked="checked"';}?> /> <?php echo $value['desc2']; ?></label>
			</td>
		</tr>
		<tr>
			<td><small><?php //echo $value['desc']; ?></small></td>
		</tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td>
		</tr>
		<!--/tr-->
		<?php
		break;
			
		case "radio_doted":
		?>
		<tr>
			<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
			<td width="80%">
				<label><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="radio" value="<?php echo $value['value']; ?>" <?php echo $selector; ?> <?php if (get_option( $value['id']) == $value['value'] || get_option( $value['id']) == ""){echo 'checked="checked"';}?> /> <?php echo $value['desc']; ?></label><br />
				<label><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>_2" type="radio" value="<?php echo $value['value2']; ?>" <?php echo $selector; ?> <?php if (get_option( $value['id']) == $value['value2']){echo 'checked="checked"';}?> /> <?php echo $value['desc2']; ?></label><br />
				<!--label><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>_3" type="radio" value="<?php echo $value['value3']; ?>" <?php echo $selector; ?> <?php if (get_option( $value['id']) == $value['value3']){echo 'checked="checked"';}?> /> <?php echo $value['desc3']; ?></label><br /-->
				

			</td>
		</tr>
		<tr>
			<td><small><?php //echo $value['desc']; ?></small></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<!--/tr-->
		<?php
		break;			
 
 		case "slider_control_panel":
		?>
		<tr>
			<td colspan="2">
			<table>
				<tr>
					<td width="41%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
					<td width="59%"></td>
				</tr>
				<tr>
					<td><small><?php echo $value['desc']; ?></small></td>
				</tr>
			
			</td>
		</tr>
		<?php 
		break;
		
		case 'dottedline':
		?>
        <tr>
            <td></td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:0px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;

		case 'displaycontactcontent':
		?>        
        <tr>
            <td>
				<strong><?php echo $value['name']; ?></strong>
			</td>
			<td><?php display_contact_content($value['id'],$shortname,'contact'); ?>
				<br><small><?php echo $value['desc']; ?></small>
			</td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;	

		case 'displaynewscontent':
		?>        
        <tr>
            <td>
				<strong><?php echo $value['name']; ?></strong>
			</td>
			<td><?php display_news_content($value['id'],$shortname,'news'); ?>
				<br><small><?php echo $value['desc']; ?></small>
			</td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;	

		case 'exclude_header_pages':
		?>
        <tr>
            <td>
				<strong><?php echo $value['name']; ?> </strong>
			</td>
			<td id="show_hide_pg">
				<?php exclude_header_pages($value['id'],$shortname); ?><small><?php echo $value['desc']; ?></small>
			</td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;

		case 'exclude_categories':
		?>
        <tr>
            <td>
				<strong><?php echo $value['name']; ?> </strong>
			</td>
			<td id="show_hide_pg">
				<?php exclude_categories($value['id'],$shortname); ?><small><?php echo $value['desc']; ?></small>
			</td>
        </tr>
		<tr>
			<td colspan="2" style="margin-bottom:5px;border-bottom:0px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td>
		</tr>
		<?php 
		break;
		
		case "toggle":
		$i++;
		?>
		<div class="slideToggle" style="background-color:#f1f1f1; padding:5px 10px;">
		<div><h3 style="cursor:pointer; font-size:1.1em; margin:0;	font-weight:bold; color:#264761; padding:10px">&rarr; <?php echo $value['name']; ?></h3>
		</span><div class="clearfix"></div></div>
		<div class="settings">
		<?php break;

		
		case "sidebar_list":
		?>	
		
		<table class="slider-box" id="demo">
			<thead>
			<tr>
				<th style="text-align:left;width:10%;">N.&nbsp;&nbsp;&nbsp;</th>
				<th style="text-align:left;width:90%;">Sidebar Name</th>

			</tr>
			</thead>
			<tbody>
			<?php
			$get_custom_options = get_option($shortname.'_sidebar_list');
			$m = 0;
			for($i = 1; $i <= 200; $i++) 
			{
				if ($get_custom_options[$shortname.'_sidebar_list_url_'.$i])
				{
					echo '
					<tr class="sidebars" rel="sidebar_'.($m+1).'"><td>'.($m+1).'</td>
					<td><input style="width: 100%;" name="'.$value['id'].'_url_'.($m+1).'" id="'.$value['id'].'_url_'.($m+1).'" 
						type="text" value="'.$get_custom_options[$shortname.'_sidebar_list_url_'.$i].'"></td>
					</tr>
					';
					$m = $m + 1;
				}
			}
			?>
			<p><?php echo $value['desc']; ?></p>
			<p>
				<input type="button" value="Add New Sidebar" onclick="addRowToTable();" />
				<!--input type="button" value="Remove" onclick="removeRowFromTable2();" /-->
			</p>
		</tbody>
		</table>
		<?php
		break;				
		
		
		case 'colorpicker':
		
			$val = $value['std'];
			$stored  = get_option($value['id']);
			if ($stored != "") { $val = $stored; }
		?>
		
       <tr>
            <td width="25%" rowspan="2" valign="top"><?php echo $value['name']; ?></td>
            <td width="75%">
			<?php
				echo '<input style="width:70px;" class="color" name="'. $value['id'] .'" id="'. $value['id'] .'" type="text" value="'. $val .'" />';
			?>

			</td>
         </tr>
        <tr>
            <td style="padding:0 0 20px 0;"><small><?php echo $value['desc']; ?></small></td>
        </tr>
		<?php 
		break;			

		case "portfolio_add":
		?>
		<tr>
            <td colspan="2">
				<strong><?php echo $value['name']; ?></strong>
				<!--br><small><?php echo $value['desc']; ?></small-->	
			</td>
        </tr>
		<tr>
			<td colspan="2"><?php echo is_pagetemplate_portfolio('portfolio-%'); ?></td>
		</tr>
		<?php
		break;		
		
} 
}
?>
</form>

<?php
}
add_action('admin_menu', 'mytheme_add_admin');


// check if is portfolio page template function
function is_pagetemplate_portfolio($pagetemplate = '') {
	global $wpdb;
	global $shortname;
	$output = '';
	$output .= '<table><tr><td colspan="3"><small>To show from all categories leave "Select category"</small></td></tr>';
	$sql = "select post_id from $wpdb->postmeta where meta_key like '_wp_page_template' and meta_value like '" . $pagetemplate . "'";
	$result = $wpdb->get_results($sql);
	foreach ($result as $value){
		$page_id = $value->post_id;
		$page_data = get_page( $page_id );
		$title = $page_data->post_title; 
		$output .=  '<tr><td>Page <strong>'.$title.'</strong></td><td>&nbsp;:&nbsp;</td><td>'.display_categories($shortname,$page_id).'</td></tr>';
	}
	$output .= '</table>';
	
	return $output;
}


// fisplay categories function					
function display_categories($shortname,$page_id) {
	//get selected page_id for portfolio
	
	$get_custom_options = get_option($shortname.'_portfolio_page_id'); 
	$get_page_id  = $get_custom_options['portfolio_to_cat_'.$page_id];	
	
	//get category name
	$get_category_name = get_option($shortname.'_portfolio_categories_'.$page_id);
	
	$output = '';
	$output .= '<select name="portfolio_to_cat_'.$page_id.'" class="postform selector">';	
	$output .= '<option value="0">&nbsp;&nbsp;&nbsp;Select category</option>';
	
	//list terms in a given taxonomy (useful as a widget for twentyten)
	$taxonomy = 'portfolio_entries';
	$tax_terms = get_terms($taxonomy);

	foreach ($tax_terms as $tax_term) {
		
		$selected_option = $tax_term->term_id;
		
		if ($get_page_id == $selected_option) { 
			$output .= '<option selected value="'.$tax_term->term_id.'">&nbsp;&nbsp;&nbsp;'.$tax_term->name.'</option>';
		} else {
			$output .= '<option value="'.$tax_term->term_id.'">&nbsp;&nbsp;&nbsp;'.$tax_term->name.'</option>';
		}
	}
	
	$output .= '</select>';	
	
	return $output;
}

// Breadcrumbs function code
function goodminimal_breadcrumbs() {
 

  if ( !is_home() && !is_front_page() || is_paged() ) {
 

 
    global $post;
	global $shortname;
    $home = home_url();
	echo '<li><a href="'.$home.'">'._e('Home', 'goodminimal').'</a> / ';
 
    if ( is_category() ) {
      global $wp_query;
	  global $shortname;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo '<li class="last">'._('Archive by category','goodminimal').' &#39;';
      single_cat_title();
	  echo '&#39;</li>';
 
    } elseif ( is_day() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> / ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> / </li>';
      echo '<li class="last">'.get_the_time('d').'</li>';
 
    } elseif ( is_month() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> / </li>';
      echo '<li class="last">'.get_the_time('F').'</li>';
 
    } elseif ( is_year() ) {
      echo '<li class="last">' . get_the_time('Y') . '</li>';
 
    } elseif ( is_single() && !is_attachment() ) {
      $cat = get_the_category(); 
	  $cat = $cat[0]; 
	  
	  
	  if (!$cat) { 
		if( get_post_type() == 'portfolio' ) { 		

		} 
	  } else {
	  	echo '<li>'.get_category_parents($cat, TRUE, ' ' . $delimiter . ' ').' / </li>';
	  }
      echo '<li class="last">';
	  the_title();
	  echo '</li>';
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> / </li>';
      echo '<li class="last">';
      the_title();
      echo '</li>';
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo '<li class="last">';
      the_title();
      echo '</li>';
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a> / </li>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo '<li class="last">';
      the_title();
      echo '</li>';
 
    } elseif ( is_search() ) {
      echo '<li class="last">' ._e( 'Search Results for', 'goodminimal' ).' &#39;' . get_search_query() . '&#39;' . '</li>';
 
    } elseif ( is_tag() ) {
      echo '<li class="last">' ._e( 'Posts tagged', 'goodminimal' ).' &#39;';
      single_tag_title();
      echo '&#39;' . '</li>';
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo '<li class="last">' ._e( 'Articles posted by', 'goodminimal' ) . $userdata->display_name . '</li>';
 
    } elseif ( is_404() ) {
      echo '<li class="last">' ._e( 'Error 404', 'goodminimal' ) . '</li>';
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

 
  }
}

function fallback_no_menu() {
	echo '<ul class="sf-menu">	
			<li><a href="'.home_url().'">Home</a></li>';
			
	$page_exclusions = get_option($shortname.'_exclude_header_pages');
	$nav_menu_pages = wp_list_pages('show_home=1&sort_column=menu_order&sort_order=asc&exclude=&title_li=&echo=0');
	$nav_menu_pages = str_replace('current_page_item','current_page_item active',$nav_menu_pages);
	echo $nav_menu_pages;
	echo '</ul>';
}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes(){
	return 'class="nextspostslink"';
}

function remove_wpautop($content) { 
    $content = do_shortcode( shortcode_unautop($content) ); 
    $content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
    return $content;
} 

class My_Walker_Nav_Menu extends Walker_Nav_Menu {
  function start_lvl(&$output, $depth) {
    $indent = str_repeat("\t", $depth);
	$indent = str_replace('current-page-item','active',$indent);
    $output .= "\n$indent<ul>\n";
  }
}

/* Add 'active' class name where is 'current_page_item' class name */
function add_last_item_class($strHTML) {
	
	if ((!is_single()) && (!is_archive()) && (!is_search()) ) {
		$intPos = strpos($strHTML,'current_page_item');
		if ($intPos) { 
			$temp_strHTML = substr($strHTML,0,$intPos) . ' active ' . substr($strHTML,$intPos,strlen($strHTML)); 
		} else { $temp_strHTML = substr($strHTML,0,$intPos) . substr($strHTML,$intPos,strlen($strHTML)); }
	} else { 
		$intPos = strpos($strHTML,'current_page_item_not_existing');
		$temp_strHTML = substr($strHTML,0,$intPos) . substr($strHTML,$intPos,strlen($strHTML));
	}
	
	$temp_strHTML = str_replace('menu-item ','',$temp_strHTML);
	$temp_strHTML = str_replace(' class="sub-menu"','',$temp_strHTML);
	$temp_strHTML = str_replace('menu-item-type-post_type menu-item-object-page ','',$temp_strHTML);		
	echo $temp_strHTML;

}
add_filter('wp_nav_menu','add_last_item_class');

//disable admin bar for site
add_filter( 'show_admin_bar', '__return_false' );
// Adding Shortcodes to Sidebar Widgets
add_filter('widget_text', 'do_shortcode');

?>
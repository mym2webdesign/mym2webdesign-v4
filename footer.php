<?php global $shortname; ?>

<footer id="footer"> 
	<section class="inner-footer clearfix">
    <div class="inner clearfix">
    	<div class="columns" id="footer_content">
        
        <div class="one_third">
    			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Column 1") ) : ?>
    			<?php endif; ?>    				
        </div><!-- END: .recent_blog -->
          
        <div class="one_third">
    			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Column 2") ) : ?>
    			<?php endif; ?>    				  
        </div><!-- END: .recent_tweet -->
          
        <div class="one_third last">
    			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Column 3") ) : ?>
    			<?php endif; ?>
        </div><!-- END: .flickr -->   
		
    	</div> <!-- END: #footer_content-->
    </div>
  </section>
    <section id="copyright" class="clearfix">
        <div class="inner clearfix">
            <div class="alignleft">
                <?php $footer_text = stripslashes(get_option($shortname."_footer_copyright")); ?>
                <span class="float_left"><?php echo $footer_text; ?></span>
             </div>
             <div class="links alignright">
                <ul>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </section>
</footer><!-- END: #footer -->

</div><!-- END: #page -->

<?php wp_footer(); ?>

<?php echo stripslashes(get_option($shortname."_tracking_code"));  ?>

</body>
</html>
<?php
/*
Template Name: Themes Showcase
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="full_width container_shadow">
			<header><h2><?php //get page section title
				if (get_post_meta($post->ID, $shortname.'_title_bar',true)) {
						echo get_post_meta($post->ID, $shortname.'_title_bar',true);
				} else { 
					echo $post->post_title; 
				} 
			?></h2></header>
							
			<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 0, 'cat' => 23 );
					$wp_query = new WP_Query($args);
					while ( have_posts() ) : the_post(); ?>
					<article class="showcase">
						<a class="showcase-hover" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
							<div class="showcase-box">
								<?php the_post_thumbnail(showcase-thumb);  ?>
								<div class="showcase-info">
									<h2 class="showcase-title"><?php the_title_attribute(); ?></h2>
									<div><p>A big, bold, beautiful theme for creatives.</p></div>
								</div>
							</div>
						</a>
					</article>
					<?php endwhile; ?>
		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
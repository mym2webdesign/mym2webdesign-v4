<?php
/*
Single Post Template: Theme
Description: This part is optional, but helpful for describing the Post Template
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="full_width container_shadow">
			<header class="clearfix">
				<div>
					<h2><?php the_title();?></h2>
				</div>
				<div class="theme-intro">
					<p class="theme-subtitle"><?php the_field('subtitle'); ?></p>
					<p><?php the_field('theme_intro'); ?></p>
				</div>
				<div class="theme-links">
					<a target="_blank" class="demo" href="<?php the_field('demo_link'); ?>">Demo</a>
					<a target="_blank" class="buy" href="<?php the_field('buy_link'); ?>">Buy</a>
				</div>
			</header>			

			<section class="clearfix">
				<div class="theme-display">
					<div class="theme-display-img">
						<img src="<?php the_field('theme_preview'); ?>" alt="" />
					</div>
				</div>
			</section>
			
			<section class="clearfix">
				
					<div class="theme-display-content">					
						<?php while ( have_posts() ) : the_post(); ?>	
						<?php the_content(); ?>
						<?php endwhile; ?>
					</div><!-- END: .post-->

					<div class="theme-display-details">
						<p><strong>Released:</strong> <?php the_date() ?></p>
						<p><strong>Last Updated:</strong> <?php the_field('last_updated'); ?></p>
						<p><strong>Theme Type:</strong> <?php the_field('theme_type'); ?></p>
					</div>
				
			</section>

		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
<?php
/*
Template Name: Work Showcase
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="container_shadow">
			<header><h2><?php //get page section title
				if (get_post_meta($post->ID, $shortname.'_title_bar',true)) {
						echo get_post_meta($post->ID, $shortname.'_title_bar',true);
				} else { 
					echo $post->post_title; 
				} 
			?></h2></header>
							
			<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 0, 'cat' => 58 );
					$wp_query = new WP_Query($args);
					while ( have_posts() ) : the_post(); ?>
					<article class="showcase">
      
  					<figure class="showcase-display">
  						<a href="<?php the_permalink(); ?>">
				    	<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail(showcase-thumb);
							}
							else {
								echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.jpg" />';
							}
							?>
							</a>
				  	</figure>   
			      <h2 class="showcase-title">
				      <a rel="bookmark" class="url entry-title" href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>          
				    </h2>
						<div class="showcase-excerpt">
				        <p><?php the_content_rss('', TRUE, '', 47); ?></p>      
							</div>
					</article>
					<?php endwhile; ?>
		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
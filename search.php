<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="container_shadow">
			<header><h2 class="page_title"><?php printf( __( 'Search Results for: %s', 'goodminimal' ), '<span>' . get_search_query() . '</span>' ); ?></h2></header>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php
				$portfolio_image_original = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false );
				$get_custom_image_url = $portfolio_image_original[0];		
				$get_custom_image_url = str_replace(home_url(),'', $get_custom_image_url);
				$image_url = get_template_directory_uri().'/functions/timthumb.php?src='.$get_custom_image_url.'&amp;w=110&amp;h=110&amp;zc=1';		
			?>
				<div class="post clearfix">
					<a href="<?php the_permalink(); ?>" class="post_thumb"><img width="110" height="110" src="<?php echo $image_url; ?>" alt="<?php the_title();?>" class="postThumb" /></a>
					<div class="inside">
						<h2 class="blog_title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
						<div class="meta clearfix"> <?php _e('Posted by', 'goodminimal'); ?> <a href="#"><?php the_author_posts_link(); ?> <?php _e('on', 'goodminimal'); ?> <a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'),get_the_time('d')); ?>"><?php echo get_the_time('F d, Y'); ?></a> <?php _e('in', 'goodminimal'); ?> <?php the_category(', ') ?> | <?php comments_popup_link(__('0 Comments', 'goodminimal'),__('1 Comment', 'goodminimal'), __('% Comments', 'goodminimal')); ?> </div>
						
						<?php the_excerpt(); ?>
						
						<p class="moreLink"><a href="<?php the_permalink(); ?>" class="small_button"><?php _e('Continue Reading...', 'goodminimal'); ?></a></p>
					</div>
				</div><!-- END: .post-->

			<?php endwhile; ?>
				<?php else : ?>
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'goodminimal' ); ?></h2>
					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'goodminimal' ); ?></p>
					<form action="/" method="get" class="search">
						<p><input type="text" name="s" id="s" class="shadow_inset" value="<?php _e('Search...', 'goodminimal'); ?>" onblur="if (this.value == ''){this.value = '<?php _e('Search...', 'goodminimal'); ?>'; }" onfocus="if (this.value == '<?php _e('Search...', 'goodminimal'); ?>') {this.value = '';}" /></p>
					</form>
					<div style="height:100%">&nbsp;</div>
			<?php endif; ?>

			<div class="pagination clearfix">
				<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } $wp_query = null; $wp_query = $temp; ?>
			</div>
		</section>

		<section id="sidebar">
		
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>
			<?php endif; ?>

			<?php 
				wp_reset_query();
				$custom = get_post_custom($post->ID);
				$current_sidebar = $custom["current_sidebar"][0];	
				
				if ($current_sidebar) {
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($current_sidebar) ) :
					endif;
				}
			?>
		</section>
		
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
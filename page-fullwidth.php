<?php
/*
Template Name: Page Full Width
*/
get_header();
?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="full_width container_shadow">
			<header><h2><?php //get page section title
				if (get_post_meta($post->ID, $shortname.'_title_bar',true)) {
						echo get_post_meta($post->ID, $shortname.'_title_bar',true);
				} else { 
					echo $post->post_title; 
				} 
			?></h2></header>
							
			<?php
				$portfolio_image_original = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false );
				$get_custom_image_url = $portfolio_image_original[0];
				$get_custom_image_url = str_replace(home_url(),'', $get_custom_image_url);
				$image_url = get_template_directory_uri().'/functions/timthumb.php?src='.$get_custom_image_url.'&amp;w=864&amp;h=216&amp;zc=1';
				
				if ($get_custom_image_url) echo '<p><img src="'.$image_url.'" alt="'.$post->post_title.'" /></p>';
			?>							
			
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
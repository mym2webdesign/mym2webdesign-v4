<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="container_shadow">
			<header><h2><?php //get page section title
				if (get_post_meta($post->ID, $shortname.'_title_bar',true)) {
						echo get_post_meta($post->ID, $shortname.'_title_bar',true);
				} else { 
					echo $post->post_title; 
				} 
			?></h2></header>
							
			<?php
				$portfolio_image_original = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false );
				$get_custom_image_url = $portfolio_image_original[0];
				$get_custom_image_url = str_replace(home_url(),'', $get_custom_image_url);
				$image_url = get_template_directory_uri().'/functions/timthumb.php?src='.$get_custom_image_url.'&amp;w=616&amp;h=216&amp;zc=1';
				
				if ($get_custom_image_url) echo '<p><img src="'.$image_url.'" alt="'.$post->post_title.'" /></p>';
			?>							
			
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</section>

		<section id="sidebar">
		
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Page Sidebar") ) : ?>
			<?php endif; ?>
			
			<?php 
				//$wp_query = null; $wp_query = $temp;
				wp_reset_query();
				$custom = get_post_custom($post->ID);
				$current_sidebar = $custom["current_sidebar"][0];	
				
				if ($current_sidebar) {
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($current_sidebar) ) :
					endif;
				}
			?>		
			
		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>
<?php get_header(); ?>

<section id="main-content">
	<div class="center_wrap">
	
		<section id="content" class="container_shadow">
			<header><h2><?php
				global $wpdb;
				$blog_name_id = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = 'blog.php'");
				echo get_post_meta($blog_name_id, $shortname.'_title_bar',true);
			?></h2></header>					
			
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="post clearfix no_bottom_border">
					<h2 class="blog_title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>					
					<div class="meta clearfix"> <?php _e('Posted by', 'goodminimal'); ?> <a href="#"><?php the_author_posts_link(); ?> <?php _e('on', 'goodminimal'); ?> <a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'),get_the_time('d')); ?>"><?php echo get_the_time('F d, Y'); ?></a> <?php _e('in', 'goodminimal'); ?> <?php the_category(', ') ?> | <?php comments_popup_link(__('0 Comments', 'goodminimal'),__('1 Comment', 'goodminimal'), __('% Comments', 'goodminimal')); ?> </div>
					
					<?php
						$portfolio_image_original = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false );
						$get_custom_image_url = $portfolio_image_original[0];		
						$get_custom_image_url = str_replace(home_url(),'', $get_custom_image_url);
						$image_url = get_template_directory_uri().'/functions/timthumb.php?src='.$get_custom_image_url.'&amp;w=616&amp;h=216&amp;zc=1';		
						if ($get_custom_image_url) echo '<p><img src="'.$image_url.'" alt="'.$post->post_title.'" /></p>';
					?>					
						
					<?php the_content(); ?>
					
					<?php comments_template( '', true ); ?>
					
				</div><!-- END: .post-->

			<?php endwhile; ?>

		</section>

		<section id="sidebar">
		
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Single Page Sidebar") ) : ?>
			<?php endif; ?>
		
			<?php			
				$custom = get_post_custom($post->ID);
				$current_sidebar = $custom["current_sidebar"][0];	
				
				if ($current_sidebar) {
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($current_sidebar) ) :
					endif;
				}
			?>
		
		</section>
		
    </div>
</section><!-- END: #main-content -->

<?php get_footer(); ?>